# Car rental service

## How to run ##
### Dependencies: ###
* Git
* Node.js 8.\*.\*
* NPM
* MySQL

### Steps: ###
1. Create DB named `car-rental` in MySQL
2. Change MySQL credentials in `knexfile.js` accordingly
3. Run `npm run setup` on command line
4. Run `npm run dev` on command line
5. Open `localhost:8080` in browser

## Development dependencies ##
* NVM
* EditorConfig
* ESLint
* MySQL