const dbConfig = {
	development: {
		client: 'mysql',
		connection: {
			host: '127.0.0.1',
			user: 'root',
			password: 'root',
			database: 'car-rental'
		},
		migrations: {
			tableName: 'migrations'
		}
	},
	production: {
		client: 'mysql',
		connection: {
			host: '',
			user: '',
			password: '',
			database: ''
		},
		pool: {
			min: 2,
			max: 10
		},
		migrations: {
			tableName: 'migrations'
		}
	}
};

module.exports = dbConfig;
