const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');
const { VueLoaderPlugin } = require('vue-loader');
const { getEnv, isProduction } = require('./server/utils');

const resolve = file => path.resolve(__dirname, file);

module.exports = {
	mode: getEnv(),
	devtool: isProduction() ? false : '#cheap-module-source-map',
	output: {
		path: resolve('./dist'),
		publicPath: '/public',
		filename: '[name].[hash].js'
	},
	resolve: {
		extensions: ['.js', '.vue', '.json'],
		alias: {
			public: resolve('./public'),
			'@': resolve('./src'),
			vue: 'vue/dist/vue.esm.js',
			vue$: 'vue/dist/vue.esm.js'
		}
	},
	module: {
		noParse: /es6-promise\.js$/, // avoid webpack shimming process
		rules: [
			{
				test: /\.vue$/,
				use: [
					{
						loader: 'vue-loader',
						options: {
							compilerOptions: {
								preserveWhitespace: false
							}
						}
					},
					{
						loader: 'vue-svg-inline-loader',
						options: {
							/* ... */
						}
					}
				]
			},
			{
				test: /\.js$/,
				loader: 'babel-loader',
				exclude: /node_modules/
			},
			{
				test: /\.(png|jpg|gif|svg)$/,
				loader: 'url-loader',
				options: {
					limit: 10000,
					name: '[name].[ext]?[hash]'
				}
			},
			{
				test: /\.styl(us)?$/,
				use: isProduction()
					? ExtractTextPlugin.extract({
							use: [
								{
									loader: 'css-loader',
									options: { minimize: true }
								},
								'stylus-loader'
							],
							fallback: 'vue-style-loader'
					  })
					: ['vue-style-loader', 'css-loader', 'stylus-loader']
			}
		]
	},
	performance: {
		maxEntrypointSize: 300000,
		hints: isProduction() ? 'warning' : false
	},
	plugins: isProduction()
		? [
				new VueLoaderPlugin(),
				new webpack.optimize.ModuleConcatenationPlugin(),
				new ExtractTextPlugin({
					filename: 'common.[chunkhash].css'
				})
		  ]
		: [new VueLoaderPlugin(), new FriendlyErrorsPlugin()]
};
