exports.seed = knex => {
	// Deletes ALL existing entries
	return knex('vehicles')
		.del()
		.then(() => {
			return knex('vehicles').insert([
				{
					manufacturer: 'Toyota',
					model: 'Yaris',
					engine: 'B 1.4',
					ac: false,
					transmission_type: 'manual',
					seats: 5,
					doors: 5,
					luggage: 1,
					image: '/public/vehicles/toyota-yaris-5d-weiss-2017.png',
					price: 69.46
				},
				{
					manufacturer: 'VW',
					model: 'Golf',
					engine: 'TD 1.4',
					ac: true,
					transmission_type: 'manual',
					seats: 5,
					doors: 5,
					luggage: 2,
					image: '/public/vehicles/vw-golf-5d-weiss-2017.png',
					price: 72.47
				},
				{
					manufacturer: 'VW',
					model: 'Golf',
					engine: 'TD 1.4',
					ac: true,
					transmission_type: 'automatic',
					seats: 5,
					doors: 5,
					luggage: 1,
					image: '/public/vehicles/vw-golf-5d-weiss-2017.png',
					price: 74.1
				},
				{
					manufacturer: 'Toyota',
					model: 'Corolla',
					engine: 'B 1.6',
					ac: true,
					transmission_type: 'manual',
					seats: 5,
					doors: 4,
					luggage: 2,
					image: '/public/vehicles/toyota-corolla-4d-grau-2017.png',
					price: 75.8
				},
				{
					manufacturer: 'Toyota',
					model: 'Auris',
					engine: 'D 1.6',
					ac: true,
					transmission_type: 'manual',
					seats: 5,
					doors: 5,
					luggage: 2,
					image: '/public/vehicles/toyota-auris-kombi-weiss-2013.png',
					price: 77.59
				},
				{
					manufacturer: 'Toyota',
					model: 'Avensis',
					engine: 'B 1.8',
					ac: true,
					transmission_type: 'automatic',
					seats: 5,
					doors: 4,
					luggage: 2,
					image: '/public/vehicles/toyota-avensis-4d-schwarz-2015.png',
					price: 79.46
				},
				{
					manufacturer: 'BMW',
					model: '318',
					engine: 'B 1.8',
					ac: true,
					transmission_type: 'automatic',
					seats: 5,
					doors: 4,
					luggage: 2,
					image: '/public/vehicles/bmw-3er-4d-blau-2018.png',
					price: 109.12
				},
				{
					manufacturer: 'BMW',
					model: '525',
					engine: 'B 2.5',
					ac: true,
					transmission_type: 'automatic',
					seats: 5,
					doors: 4,
					luggage: 2,
					image: '/public/vehicles/bmw-5er-4d-weiss-2017.png',
					price: 118.62
				},
				{
					manufacturer: 'BMW',
					model: 'X5',
					engine: 'B 5.0',
					ac: true,
					transmission_type: 'automatic',
					seats: 5,
					doors: 5,
					luggage: 2,
					image: '/public/vehicles/bmw-x5m-50d-weiss-2015.png',
					price: 140.44
				}
			]);
		});
};
