const should = require('should');
const sinon = require('sinon');
const proxyrequire = require('proxyquire');

describe('index', () => {
	let proxy;
	let res;
	let configMock;
	let expressMock;
	let rendererMock;
	let loggerMock;

	beforeEach(() => {
		res = {
			end: sinon.spy(),
			writeHead: sinon.spy(),
			setHeader: sinon.spy()
		};

		configMock = {
			port: 1234
		};

		expressMock = {
			use: sinon.spy(),
			get: sinon.stub().yields({}, res),
			listen: sinon.stub().yields()
		};

		rendererMock = {
			createBundleRenderer: sinon.stub().returns({
				renderToString: sinon.spy()
			})
		};

		loggerMock = {
			error: sinon.spy(),
			info: sinon.spy()
		};

		proxy = {
			'./server/config': configMock,
			'./server/dev-server': sinon
				.stub()
				.withArgs(expressMock, 'path/to/file', sinon.spy())
				.yields('{}', '')
				.resolves(res),
			'vue-server-renderer': rendererMock,
			express: sinon.stub().returns(expressMock),
			'./server/logger': loggerMock
		};

		proxyrequire('./server', proxy);
	});

	it('initializes Express.js', () => {
		should.equal(proxy.express.callCount, 1);
	});

	it('calls Express.js .use() method 7 times', () => {
		should.equal(expressMock.use.callCount, 7);
	});

	it('calls Express.js .get() method', () => {
		should.equal(expressMock.get.callCount, 1);
	});

	it('calls Express.js .listen() method with port 1234', () => {
		should.equal(expressMock.listen.firstCall.args[0], 1234);
	});
});
