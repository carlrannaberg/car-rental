module.exports = {
	root: true,
	env: {
		browser: true,
		node: true,
		mocha: true
	},
	extends: ['airbnb-base', 'plugin:vue/essential', 'prettier', 'prettier/vue'],
	plugins: ['prettier', 'script-tags'],
	rules: {
		'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
		'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
		'prettier/prettier': ['error', {
			singleQuote: true,
			printWidth: 100
		}],
		'prefer-rest-params': 'off',
		'no-warning-comments': 'off',
		'no-tabs': 'off',
		'one-var': 'off',
		'comma-dangle': 'off',
		'object-curly-spacing': 0,
		'no-shadow': ['error', { builtinGlobals: false, hoist: 'functions', allow: [] }],
		'arrow-body-style': 0,
		'consistent-return': 0,
		'prefer-promise-reject-errors': ['error', { allowEmptyReject: true }],
		'global-require': 'off',
		'no-underscore-dangle': ['error', { allow: ['__INITIAL_STATE__'] }],
		'no-param-reassign': [
			'error',
			{
				'props': true,
				'ignorePropertyModificationsFor': [
					'state',
					'acc',
					'e',
					'ctx',
					'req',
					'request',
					'res',
					'response',
					'$scope'
				]
			}
		]
	},
	overrides: [
		{
			files: ['*.spec.js'],
			rules: {
				'no-unused-expressions': 'off'
			}
		},
		{
			files: ['*.vue'],
			rules: {
				'import/no-unresolved': 'off'
			}
		}
	],
	parserOptions: {
		parser: 'babel-eslint'
	}
};
