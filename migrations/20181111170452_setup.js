exports.up = async knex => {
	await knex.schema.createTable('users', table => {
		table.increments('id');
		table.timestamps(true, true);
		table
			.enu('status', ['unverified', 'verified', 'declined', 'disabled'])
			.defaultTo('unverified');
		table.string('email').unique();
		table.string('password');
		table.string('name');
		table.string('phone');
		table.string('address');
		table.string('city');
		table.string('country');
		table.string('ZIP');
	});

	await knex.schema.createTable('vehicles', table => {
		table.increments('id');
		table.timestamps(true, true);
		table.enu('status', ['active', 'inactive']).defaultTo('active');
		table.string('manufacturer');
		table.string('model');
		table.string('engine');
		table.boolean('ac').defaultTo(false);
		table.enu('transmission_type', ['manual', 'automatic']).defaultTo('manual');
		table.integer('seats');
		table.integer('doors');
		table.integer('luggage');
		table.string('description');
		table.string('image');
		table.string('price');
		table.string('vin');
		table.string('reg_nr');
	});

	await knex.schema.createTable('orders', table => {
		table.increments('id');
		table.timestamps(true, true);
		table
			.integer('user_id')
			.unsigned()
			.notNullable();
		table
			.integer('vehicle_id')
			.unsigned()
			.notNullable();
		table
			.enu('status', ['unconfirmed', 'confirmed', 'in_possession', 'returned'])
			.defaultTo('in_possession');
		table.datetime('start');
		table.datetime('end');

		table
			.foreign('user_id')
			.references('id')
			.inTable('users');
		table
			.foreign('vehicle_id')
			.references('id')
			.inTable('vehicles');
	});
};

exports.down = async knex => {
	await knex.schema.dropTable('orders');
	await knex.schema.dropTable('users');
	await knex.schema.dropTable('vehicles');
};
