const should = require('should');
const proxyrequire = require('proxyquire');

describe('utils/knex', () => {
	let proxy;
	let knex;

	beforeEach(() => {
		proxy = {};

		knex = proxyrequire('./knex.js', proxy);
	});

	describe('utils.knex', () => {
		it('returns "knex" instance', () => {
			should(knex).be.an.Object;
			should(knex.insert).be.a.Function;
		});
	});
});
