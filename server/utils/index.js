const logger = require('../logger');

const utils = {
	/**
	 * Get current Node environment
	 * @return {String} Returns NODE_ENV value or "development"
	 */
	getEnv: () => process.env.NODE_ENV || 'development',
	/**
	 * Is current Node environment "production"
	 * @return {Boolean} Returns true if current environment is "production"
	 */
	isProduction: () => utils.getEnv() === 'production',
	/**
	 * Read file with injected filesystem utility
	 * @param  {Object} filesystem Filesystem utility like "fs", "fs-extra", "mfs"
	 * @param  {String} filePath   Path to file expected to be read
	 * @return {Buffer}            Returns file as buffer
	 */
	readFile: (filesystem, filePath) => {
		try {
			return filesystem.readFileSync(filePath, 'utf-8');
		} catch (error) {
			logger.error(error);
		}
	}
};

module.exports = utils;
