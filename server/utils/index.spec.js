const should = require('should');
const sinon = require('sinon');
const proxyrequire = require('proxyquire');

describe('utils/index', () => {
	describe('utils.getEnv', () => {
		const utils = require('./index');

		afterEach(() => {
			delete process.env.NODE_ENV;
		});

		it('returns "development" when NODE_ENV is missing', () => {
			const ENV = utils.getEnv();

			should(ENV).equal('development');
		});

		it('returns "production" when NODE_ENV is "production"', () => {
			process.env.NODE_ENV = 'production';

			const ENV = utils.getEnv();

			should(ENV).equal('production');
		});
	});

	describe('utils.isProduction', () => {
		const utils = require('./index');

		afterEach(() => {
			delete process.env.NODE_ENV;
		});

		it('returns "false" when NODE_ENV is missing', () => {
			const isProduction = utils.isProduction();

			should(isProduction).be.False();
		});

		it('returns "true" when NODE_ENV is "production', () => {
			process.env.NODE_ENV = 'production';

			const isProduction = utils.isProduction();

			should(isProduction).be.True();
		});
	});

	describe('utils.readFile', () => {
		let loggerMock;
		let proxy;
		let utils;

		beforeEach(() => {
			loggerMock = {
				error: sinon.spy()
			};

			proxy = {
				'../logger': loggerMock
			};

			utils = proxyrequire('./index', proxy);
		});

		it('returns Buffer', () => {
			const filePath = './index.js';
			const filesystem = {
				readFileSync: sinon.stub().returns(Buffer.from(filePath))
			};
			const file = utils.readFile(filesystem, filePath);

			should(file instanceof Buffer).be.True();
		});

		it('calls logger.error() when readFileSync throws', () => {
			const filePath = './abc.txt';
			const filesystem = {
				readFileSync: sinon.stub().throws(new Error('No file found.'))
			};

			utils.readFile(filesystem, filePath);

			should(proxy['../logger'].error.called).be.True();
		});
	});
});
