const Knex = require('knex');
const { database } = require('../config');

const knex = Knex(database);

module.exports = knex;
