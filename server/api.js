const knex = require('./utils/knex');

module.exports = {
	getVehicles(options = {}) {
		return knex
			.select()
			.from('vehicles')
			.where(options);
	}
};
