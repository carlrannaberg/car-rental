const { getEnv } = require('../utils/');
const dbConfig = require('../../knexfile');

const config = {
	development: {
		port: 8080,
		database: dbConfig.development
	},
	production: {
		port: 80,
		database: dbConfig.production
	}
};

module.exports = config[getEnv()];
