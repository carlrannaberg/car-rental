import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

// Assume we have a universal API that returns Promises
// and ignore the implementation details
const fetchVehiclesFromAPI = (/* options */) => {
	return axios.get('./api/vehicles');
};

export default () => {
	return new Vuex.Store({
		state: {
			drawerOpen: false,
			selectedLocation: null,
			datePickup: null,
			dateDropoff: null,
			vehicles: {}
		},
		actions: {
			fetchVehicles({ commit }, options) {
				// return the Promise via `store.dispatch()` so that we know
				// when the data has been fetched
				return fetchVehiclesFromAPI(options)
					.then(response => {
						response.data.forEach(item => {
							commit('setVehicle', item);
						});
					})
					.catch(error => {
						console.log(error.message);
					});
			}
		},
		mutations: {
			toggleDrawer(state, isOpen) {
				state.drawerOpen = isOpen;
			},
			updateSelectedLocation(state, location) {
				state.selectedLocation = location;
			},
			updateDatePickup(state, date) {
				state.datePickup = date;
			},
			updateDateDropoff(state, date) {
				state.dateDropoff = date;
			},
			setVehicle(state, item) {
				Vue.set(state.vehicles, item.id, item);
			}
		}
	});
};
