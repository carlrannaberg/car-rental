import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default () => {
	return new Router({
		mode: 'history',
		base: process.env.BASE_URL,
		routes: [
			{
				path: '/',
				name: 'home',
				component: () => import('./pages/Home.vue')
			},
			{
				path: '/search',
				name: 'search',
				component: () => import('./pages/Search.vue')
			},
			{
				path: '/about',
				name: 'about',
				component: () => import('./pages/About.vue')
			},
			{
				path: '*',
				component: () => import('./pages/NotFound.vue')
			}
		]
	});
};
