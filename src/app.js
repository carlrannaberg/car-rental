import Vue from 'vue';
import Vuetify from 'vuetify';
import colors from 'vuetify/es5/util/colors';
import { sync } from 'vuex-router-sync';
import App from './App.vue';
import createRouter from './router';
import createStore from './store';
import registerComponents from './register-components';
import './styles/main.styl';

Vue.use(Vuetify, {
	theme: {
		primary: colors.indigo.lighten1,
		secondary: colors.red.lighten4,
		accent: colors.teal.lighten1
	}
});

Vue.use(registerComponents);

Vue.mixin({
	beforeMount() {
		const { asyncData } = this.$options;

		if (asyncData) {
			// assign the fetch operation to a promise
			// so that in components we can do `this.dataPromise.then(...)` to
			// perform other tasks after data is ready
			this.dataPromise = asyncData({
				store: this.$store,
				route: this.$route
			});
		}
	}
});

export default (/* context */) => {
	const router = createRouter();
	const store = createStore();

	sync(store, router);

	const app = new Vue({
		router,
		store,
		render: h => h(App)
	});

	return { app, router, store };
};
