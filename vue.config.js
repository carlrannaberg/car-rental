const path = require('path');
const webpackClientConfig = require('./webpack.client.config');
const webpackServerConfig = require('./webpack.server.config');

const TARGET_NODE = process.env.WEBPACK_TARGET === 'node';
const webpackConfig = TARGET_NODE ? webpackServerConfig : webpackClientConfig;

module.exports = {
	outputDir: path.resolve(__dirname, './dist'),
	configureWebpack: () => webpackConfig
};
